
/**
 * @param string 自动输出的文本
 * @param string 容器
 * @param string 控制器
 */
var utils = {
    autoWrite: function (text,container,control){
        let i = 0;
        let timer = null;
        let clear = null;
        let value = '';
        let box ;
        let handle = document.getElementById(control);
        let txt = document.getElementById(text);
      
        handle.addEventListener('click',init);
        function init(){
            clear = null;
            if ( txt === null){
                console.log(text);
                value = text;
            } else {
                console.log([txt]);
                value = txt.value;
            }
            box = document.getElementById(container);
            timer = setInterval(show, 200);
        }
        function show(){
            i++;
            let str = value.substring(0,i);
            console.log(str);
            box.innerHTML = str;
            if ( i == value.length) {
                clear = clearInterval(timer);
            }
        }
        return this.autoWrite;
    }
}

